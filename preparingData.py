import imports
# import configuration

def readData():
    ''' reading the logs from file '''
    with open('hetrogenious_logs.log') as file:
        logs = [ line[:-2] for line in file.readlines()]
    return logs
def cleanData(rawLogs):
    ''' Cleaning the raw logs, removing noise etc '''
    pattern = r'[0-9\@\#\$\%\*\(\)\-\'\:\/\.]'
    corpus, ticks = list(), list()
    for log in rawLogs:
        matches = re.sub(pattern,'',log).replace(' ','')
        #print(matches)
        corpus.append(matches)
    return corpus

# Visulising the ponits
#
def visulisePoints(points):
    ''' Take data pints and visulize them '''
    chart = sns.countplot(points, palette=HAPPY_COLORS_PALETTE)
    plt.title("Number of texts")
    chart.set_xticklabels(points, rotation=80, horizontalalignment='right');


class prepareData:
  DATA_COLUMN = "text"

  def __init__(self, train, test, tokenizer: FullTokenizer, max_seq_len=192):
    self.tokenizer = tokenizer
    self.max_seq_len = 0

    train, test = map(lambda df: df.reindex(df[IntentDetectionData.DATA_COLUMN].str.len().sort_values().index), [train, test])

    ((self.train_x), (self.test_x)) = map(self._prepare, [train, test])

    print("max seq_len", self.max_seq_len)
    self.max_seq_len = min(self.max_seq_len, max_seq_len)
    self.train_x, self.test_x = map(self._pad, [self.train_x, self.test_x])

  def _prepare(self, df):
    x = []

    for _, row in tqdm(df.iterrows()):
      text = row[IntentDetectionData.DATA_COLUMN]
      tokens = self.tokenizer.tokenize(text)
      tokens = ["[CLS]"] + tokens + ["[SEP]"]
      token_ids = self.tokenizer.convert_tokens_to_ids(tokens)
      self.max_seq_len = max(self.max_seq_len, len(token_ids))
      x.append(token_ids)

    return np.array(x)

  def _pad(self, ids):
    x = []
    for input_ids in ids:
      input_ids = input_ids[:min(len(input_ids), self.max_seq_len - 2)]
      input_ids = input_ids + [0] * (self.max_seq_len - len(input_ids))
      x.append(np.array(input_ids))
    return np.array(x)
def makeDataframe(x):
    df = pd.DataFrame(x)
    df.columns = ['text']
    return df

if __name__=='__main__':
    rawlogs = readData()
    corpus = cleanData(rawlogs)
    visulisePoints(corpus[:200]) # visulize first 200 data points
    train_x, test_x = train_test_split(corpus, shuffle=True, train_size=0.90)
    df = makeDataframe(train_x)
    df1 = makeDataframe(test_x)
    tokenizer = None
    data = prepareData(df,df1,tokenizer,max_seq_len=128)
    print(data.train_x)
    print(data.max_seq_len)
