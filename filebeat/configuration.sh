

sudo docker pull docker.elastic.co/beats/filebeat:7.9.3

sudo docker run \
docker.elastic.co/beats/filebeat:7.9.3 \
setup -E setup.kibana.host=172.21.0.1:5601 \
-E output.elasticsearch.hosts=["172.21.0.1:9200"]


sudo docker run -d \
  --name=filebeat \
  --user=root \
  --volume="$(pwd)/filebeat.yml:/usr/share/filebeat/filebeat.yml:ro" \
  --volume="/var/log/logs_dataset:/var/log/custom:ro" \
  docker.elastic.co/beats/filebeat:7.9.3 filebeat -e -strict.perms=false \
  -E output.elasticsearch.hosts=["172.21.0.1:9200"]


